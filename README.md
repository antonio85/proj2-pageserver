Author: Antonio Silva Paucar
email: antonios@uoregon.edu

A simple webserver using flask that serves html files located at the "./templates" directory and the css in "./static". 
The program will check if the file it is present in this folder and return the render of the html and css code. 
If it is not present, the server will send a 404 signal plus show a missing file page. 
If the file starts with "~", "//" or "..", a 403 signal will be sent and forbidden page would be render in the web browser.

I am providing a Makefile

To start the server, Just type "make all n=web" to build it and start it.
