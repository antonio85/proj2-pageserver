from flask import Flask, render_template, request
app = Flask(__name__)


@app.route('/')#by default, if no parameters are added, the server shows the index page or trivia.html in this case.
def index():#when only the localhost:5000 is entered. It send it to index.
    try:#try to find a file, if not, except.
        return render_template('trivia.html'), 200
    except:
        return not_found(404,"index")

@app.route('/<path:url>')#grabs the url after "5000/" and search it.
def folder(url):
    try:#
        return render_template(url, title='Hola'), 200#sends the website and the code.
    except:
        if "//" in url or ".." in url or "~" in url:
            return forbidden(403,url)
        return not_found(404,url)

@app.errorhandler(404)
def not_found(e, file):
    return render_template('404.html', title=file), e

@app.errorhandler(403)
def forbidden(e, file):
    return render_template('403.html', title=file), e

if __name__ == "__main__":

    app.run(debug=True,host='0.0.0.0')
